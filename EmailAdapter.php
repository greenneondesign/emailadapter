<?php	
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	require_once(dirname(dirname(__FILE__)) . '/phpmailer/src/Exception.php');
	require_once(dirname(dirname(__FILE__)) . '/phpmailer/src/PHPMailer.php');
	require_once(dirname(dirname(__FILE__)) . '/phpmailer/src/SMTP.php');
	
	/**
	 *	We use a PHPMailer instance configured via our config mechanism to send
	 *		emails immediately in from the UI. In the future, once backend workers
	 *		are available we may want to instead defer the send to a worker therefore
	 *		we provide a method enqueue_message that wraps PHPMailer->send but in the
	 *		future could write to the db
	 */
	class EmailAdapter extends PHPMailer {
		private static $emailer;	/** The singelton instance */
		
		public function __construct() {
			parent::__construct(true);
		
			$settings = \Application::application()->getConfigurationSection('email');
			if($settings != NULL) {
				$this->setFrom($settings['from_address'], $settings['from_name']);
				if($settings['use_smtp']) {
					$this->isSMTP();
					$this->Host = $settings['smtp_server'];
					$this->Port = $settings['smtp_port'];
					if(isset($settings['smtp_transport_security'])) {
						$this->SMTPSecure = $settings['smtp_transport_security'];
					}
					if(isset($settings['smtp_auth']) && $settings['smtp_auth']) {
						$this->SMTPAuth = true;
						$this->Username = $settings['smtp_auth_username'];
						$this->Password = $settings['smtp_auth_password'];
					}
				}
			} else {
				// throw exception
				die('Unable to configure sending of email');
			}
		}
		
		/**
		 *	enqueue_message - enqueues an email message to be sent
		 *
		 *	@param {array<string|array>} to_addresses - an array of email address that should
		 *		receive the message (To:)
		 *	@param {?array<string|array>} cc_addresses - an array of email addresses that should
		 *		receive the message (CC:)
		 *	@param {string} subject - the message subject
		 *	@param {string} plain_text_message - the message body
		 *	@param {?string} rich_text_message - the message body as html
		 */
		public static function enqueue_message($to_addresses, $cc_addresses, $subject, $plain_text_message, $rich_text_message = null) {
			if(!self::$emailer) {
				self::$emailer = new EmailAdapter();
			}
			
			if(is_array($to_addresses)) {
				self::$emailer->clearAddresses();
				foreach($to_addresses as $address) {
					if(is_array($address)) {
						self::$emailer->addAddress($address['email'], $address['name']);
					} else {
						self::$emailer->addAddress($address);
					}
				}
			} else {
				throw new Exception('Invalid parameter: $to_addresses must be an array of email addresses');
			}
			
			if(is_array($cc_addresses)) {
				foreach($cc_addresses as $address) {
					if(is_array($address)) {
						self::$emailer->addCC($address['email'], $address['name']);
					} else {
						self::$emailer->addCC($address);
					}
				}
			}

			if(NULL === $subject){
				throw new Exception('Invalid parameter: $subject must be provided');
			} else {
				self::$emailer->Subject = $subject;
			}
				
			if(NULL === $plain_text_message) {
				throw new Exception('Invalid parameter: $plain_text_message must be provided');
			} else {
				if(NULL === $rich_text_message) {
					// Plain Text Email
					self::$emailer->isHTML(false);
					self::$emailer->Body = $plain_text_message;
				} else {
					// HTML Encoded Email
					self::$emailer->isHTML(true);
					self::$emailer->Body = $rich_text_message;
					self::$emailer->AltBody = $plain_text_message;
				}
			}
			
			try {
				self::$emailer->send();				
			} catch(Exception $e) {
				Notification::error($e->getMessage());
			}
		}
	}
	
	/**
	 *	enqueue_message - enqueues an email message to be sent
	 *
	 *	@param addresses - an array of strings each representing an email address
	 *		that should receive the message (To:)
	 *	@param subject - the message subject
	 *	@param message - the message body
	 */
	function enqueue_email($addresses, $subject, $message) {
		EmailAdapter::enqueue_message($addresses, null, $subject, $message);
	}
	
	/**
	 *	enqueue_using_template - enqueues an email message to be sent
	 *
	 *	@param template
	 *	@param context
	 */
	function enqueue_using_template($template, $context) {
		// we need the parser to tease apart the email for PHP Mailer
		require_once(dirname(dirname(__FILE__)) . '/plancake-email-parser/PlancakeEmailParser.php');
		
		// unpack context... this is dangerous a malicious template could overwrite variables in the local stack frame 
		if(is_array($context)) {
			foreach($context as $key => $value) { $$key = $value; }					
		}
		
		ob_start();
		include($template);
		$text = ob_get_clean();
		$parser = new \PlancakeEmailParser($text);
		EmailAdapter::enqueue_message($parser->getTo(), $parser->getCc(), $parser->getSubject(), $parser->getPlainBody(), $parser->getHTMLBody());
	}
?>